---
sidebar_position: 1
title: Introduction
---

# Introduction to the Gaia-X Web3 ecosystem and GEN-X Network
This document serves as an introdcution and overview Gaia-X Web3 ecosystem and GEN-X Network


## What is the Gaia-X Web3 ecosystem "Pontus-X"?


The Gaia-X Web3 ecosystem "Pontus-X" is the first fully decentralized data space in Gaia-X, enabling technical data-sovereign data sharing and data monetization, while adhering to the rules of the Gaia-X Trust Framework.

It allows companies and institutions to consume and offer data services in the Gaia-X network, while remaining in control of their IP and sensitive data, thus meeting GDPR and IT-security requirements. A place to publish, discover, select, and consume data service offerings are our portals, which provide a user-interface to interact with core federation service functions.

Within Pontus-X, you can utilize federated analysis, machine learning (ML) algorithms and AI (Artificial Intelligence) services to extract valuable information from data offerings or to offer access to your sensitive data without the risk of exposing it to unnecessary third-party or compliance risks. One of the core features of the Gaia-X Web3 ecosystem is a data access mechanism called Compute-to-Data (CtD). CtD enables data owners to grant only compute access to their data without the need to create copies in other environments you do not control. The data itself can remain with the data owner in a secured environment to minimize the risk of data-leaks.

The Gaia-X Web3 ecosystem "Pontus-X" started as "Minimal Viable Gaia-X" during the first Gaia-X hackathon in 2021 and has been continuously developed and evolved since then.

Today "Pontus-X" features a full set of federation services and is connected to the Gaia-X compliance service and registry. It features over 100 service offerings from all over Europe and across the Gaia-X domains.

This ecosystem allows all participants to become a federator without relying on any centralized component and central point of failure and control in its stack. All participants can contribute and share the common infrastructure while any participant or federator can stop participating at any point in time, without stopping or interrupting the function of the ecosystem.

## What is GEN-X?

GEN-X is a community-driven Pan-European network for the Gaia-X Web3 ecosystem, a first fully decentralized data space in Gaia-X. It is owned by no one and open to everyone, entirely run and governed by Gaia-X community members dedicated to the Gaia-X Association for Data and Cloud (AISBL) Trust Framework.

Early network validators for GEN-X include Arsys (Spain), deltaDAO AG (Germany), EuProGigant (Austria/Germany), Exoscale (Switzerland/Austria/Germany), IONOS Cloud (Germany), WOBCOM GmbH (Germany) and the Staatsbibliothek zu Berlin (Germany), with more to follow.


As GEN-X is a customizable blockchain built with Polygon Edge and leveraging Polygon Supernets, it enables the creation of industry-specific data spaces that can be tailored to individual needs and use cases of consortia, SMEs, or public institutions.

By providing an open and secure infrastructure for data transactions, GEN-X enables businesses and institutions to have more control over their data and how it is used, while also ensuring compliance with the Gaia-X Trust Framework. This helps to foster trust and interoperability within the Gaia-X Web3 ecosystem and allows for data spaces that can be used by multiple parties in a decentralized and autonomous manner without creating further data silos.

For further information we have created a [GEN-X documentation](https://docs.genx.minimal-gaia-x.eu/docs/intro/) and also maintain an [up-to-date list](https://docs.genx.minimal-gaia-x.eu/docs/Community/participants) of participants of the network.

## Why should I care at all

The European data economy is expected to grow from 301 billion in 2018 to 829 billion in 2025. Data sharing and thus the effective development of AI solutions is vital for European companies and their competitiveness in the global market. Nevertheless, there are many reasons why data sharing continues to be carried out only on a small scale and with little sensitive data. Companies are concerned about losing control over their data and thus their intellectual property. Furthermore, there are high penalties for GDPR-violations. Companies do not trust data platforms that they do not control and whose participants they do not know.

The Gaia-X Web3 ecosystem and Compute-to-Data solve these problems and bring trust to data spaces with its decentralized GEN-X network, which has no controlling central authority and by following the rules of the Gaia-X Trust Framework.

## What are my benefits?

The Gaia-X Web3 ecosystem enables data monetization of sensitive data without losing control over it. This opens a whole new range of revenue opportunities. It is irrelevant where the data is located, any source up to edge devices can be connected to the data space. An open, transparent and ecosystem in the Gaia-X network opens a significantly larger amount of data and thus many new use cases and AI solutions in many different verticals. It creates the conditions for an outburst of data within the European market.